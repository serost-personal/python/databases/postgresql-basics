from sqlalchemy import create_engine, ForeignKey, Column, String, Integer
from sqlalchemy.orm import sessionmaker, declarative_base

Base = declarative_base()


# class that symbolizes table in a database
class User(Base):
    # what table name should be? (CREATE TABLE IF NOT EXISTS ...)
    __tablename__ = "users"

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    first_name = Column("first_name", String(length=50), nullable=False)
    last_name = Column("last_name", String(length=50), nullable=False)
    age = Column("age", Integer, nullable=False)

    def __init__(self, first_name: str, last_name: str, age: int):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def __repr__(self) -> str:
        return (
            "(id: {id}, "
            + "first_name: {first_name}, "
            + "last_name: {last_name}, "
            + "age: {age})"
        ).format(
            id=self.id,
            first_name=self.first_name,
            last_name=self.last_name,
            age=self.age,
        )


def main():
    dsn = "postgresql://{username}:{password}@{host}:{port}/{dbname}".format(
        username="postgres",
        password="password123",  # see docker-compose.yaml
        host="localhost",
        port=5432,
        dbname="postgres",
    )

    engine = create_engine(dsn)
    engine.connect()

    # create table if not present.
    Base.metadata.create_all(engine)

    Session = sessionmaker(engine)
    session = Session()

    # create users via python class instances
    u1 = User("John", "Doe", 18)
    u2 = User("Jane", "Doe", 20)
    u3 = User("Dave", "Doe", 19)

    # add queries to queue, do not execute yet...
    session.add(u1)
    session.add(u2)
    session.add(u3)

    # execute querries
    session.commit()

    all_users: list[User] = session.query(User).all()
    for u in all_users:
        print(u)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print("Exception in main()! Message:\n{}".format(e))
