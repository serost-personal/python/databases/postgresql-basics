import psycopg2


def main():
    # usually those are passed through environment variables,
    # but will do the job for now.
    dsn = "postgresql://{username}:{password}@{host}:{port}/{dbname}".format(
        username="postgres",
        password="password123",  # see docker-compose.yaml
        host="localhost",
        port=5432,
        dbname="postgres",
    )
    try:
        conn = psycopg2.connect(dsn)

        # cur is used to store a result of an insert statement.
        # but for this very query, it seems pointless
        cur = conn.cursor()
        # insert will fail second time!
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS mytable(
                id INT PRIMARY KEY,
                other VARCHAR(50)
            );
            INSERT INTO mytable (id, other) VALUES
                (1, 'Hello'),
                (2, 'Wonderful'),
                (3, 'World');
            SELECT * FROM mytable;
            """
        )
        # cur stores result of .fetchall() call.
        # if you need multiple selects at the same connection,
        # use multiple cursors
        for row in cur.fetchall():  # list of tuples (rows)
            print(row)
        cur.close()
        conn.commit()  # commit all changes
        conn.close()
    except Exception as e:
        print("Connection failed! Exception text:\n{}".format(e))


if __name__ == "__main__":
    main()
